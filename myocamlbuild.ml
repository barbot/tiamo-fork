
open Ocamlbuild_plugin (* open the main API module *)
open Command

(* TODO find a cleaner way to retrieve these informations from the environment
 *      maybe the CXX_FLAGS should be given as arguments to the 'c++' tag,
 *      thus allowing per-file CXX flags.
 *)
let cxx = "g++"

(**
 * Default CXX flags:
    * -xc++ to be sure to compile .c files as c++
    * -fPIC to ensure position-independent code
 *)
let cxx_flags =
  let env_cxx = 
    try Some (Sys.getenv "CXX_FLAGS")
    with | Not_found -> None
  in
  match env_cxx with 
  | None -> ["-xc++"; "-fPIC"]
  | Some x -> ["-xc++"; "-fPIC"; x]
  
(* a helper function *)
(* prefix every c++ flags with -ccopt *)
let ocaml_cxx_flags = fun flags ->
  let camlflags = List.flatten (List.map (fun opt -> ["-ccopt"; opt]) flags) in
  atomize camlflags

let _ =
  dispatch begin function
    | After_rules ->

        (* a custom noassert flag, only for compilation, not preprocessing *)
        flag ["compile";"noassert"] (A"-noassert");

        (* set proper compilation flags for c++ *)
        flag ["c++"] (S[A"-cc"; A cxx; ocaml_cxx_flags cxx_flags]);
        flag ["c++";"debug"] (ocaml_cxx_flags ["-g"]);
        flag ["c++";"noassert"] (ocaml_cxx_flags ["-DNDEBUG"]);

        (* hard-coding the C file and utap library seems the easiest way
         * to avoid problems in object ordering at link time.
         * Unfortunately, the dependency is not handled through ocamlbuild,
         * but in the Makefile directly.
         *)
        let xml_flags =
          try
            Sys.getenv "XML_LINK_FLAGS"
          with Not_found -> "-lxml2"
        in
        flag ["ocaml"; "link"; "native"]
        (S[ P"src/ctime.o";
            P"src/timedAutomatonBuilder.o";
            A"-cclib"; A("-L"^(Pathname.pwd / "utap/src"));
            A"-cclib"; A"-lutap";
            A"-cclib"; A xml_flags
          ]);

    | _ -> ()
  end

