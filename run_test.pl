#! /usr/bin/perl

print "Running test : $ARGV[0] \n";
open IN, "< $ARGV[0]";


my $title = <IN>;
chomp $title;
my $call = <IN>;
chomp $call;

my $nominal;
my %expected;

while (my $line = <IN>) {
  chomp $line;
  if ($line =~ s/Result of verification is //) {
    $nominal = $line;
  }
  # gather stats
  if ($line =~ s/^discrete states://) {
    my @splitline = split(' ',$line);
    $expected{"ds"} = $splitline[0];
  }
  if ($line =~ s/^states seen://) {
    my @splitline = split(' ',$line);
    $expected{"ss"} = $splitline[0];
  }
  if ($line =~ s/^states explored://) {
    my @splitline = split(' ',$line);
    $expected{"se"} = $splitline[0];
  }
  if ($line =~ s/^incl tests://) {
    my @splitline = split(' ',$line);
    $expected{"incl"} = $splitline[0];
  }
  if ($line =~ s/^pos\. incl tests://) {
    my @splitline = split(' ',$line);
    $expected{"pincl"} = $splitline[0];
  }
}

close IN;

# Now run the tool# 

# print "syscalling : $call \n";
print "##teamcity[testStarted name='$title']\n";

my $tested;

my @outputs = ();
my @results = `$call 2>&1;`;

while (my $line = shift(@results)) {
  chomp $line;
  push (@outputs,$line);
  if ($line =~ s/Result of verification is //) {
    $tested = $line;
  }
  # report some stats to teamcity
  if ($line =~ s/^discrete states://) {
    my @splitline = split(' ',$line);
    my $exp = $expected{"ds"};
    if ($splitline[0] != $exp) {
      print "##teamcity[message text='number of discrete states $splitline[0] differs from nominal $exp' status='WARNING']\n";
    }
  }
  if ($line =~ s/^states seen://) {
    my @splitline = split(' ',$line);
    my $exp = $expected{"ss"};
    if ($splitline[0] != $exp) {
      print "##teamcity[message text='number of states seen $splitline[0] differs from nominal $exp' status='WARNING']\n";
    }
  }
  if ($line =~ s/^states explored://) {
    my @splitline = split(' ',$line);
    my $exp = $expected{"se"};
    if ($splitline[0] != $exp) {
      print "##teamcity[message text='number of states explored $splitline[0] differs from nominal $exp' status='WARNING']\n";
    }
  }
  if ($line =~ s/^incl tests://) {
    my @splitline = split(' ',$line);
    my $exp = $expected{"incl"};
    if ($splitline[0] != $exp) {
      print "##teamcity[message text='number of inclusion tests $splitline[0] differs from nominal $exp' status='WARNING']\n";
    }
  }
  if ($line =~ s/^pos\. incl tests://) {
    my @splitline = split(' ',$line);
    my $exp = $expected{"pincl"};
    if ($splitline[0] != $exp) {
      print "##teamcity[message text='number of successful inclusiontests $splitline[0] differs from nominal $exp' status='WARNING']\n";
    }
  }
}

# retrieve the exit value of the test
my $failure;
if ($? == 0) {
  $failure = 0;
} else {
  $failure = 1;
}

my $tc_output = join("|n", @outputs);
# escape characters for TeamCity
$tc_output =~ s/\[/\|\[/g;
$tc_output =~ s/\]/\|\]/g;
$tc_output =~ s/'/\|'/g;
my $reg_output = join("\n", @outputs);
if ( $failure ) {
  print "$reg_output\n";
  print "\n##teamcity[testFailed name='$title' message='test did not exit properly' details='$tc_output' expected='$nominal' actual='$tested'] \n";
  print "Expected :  $nominal  Obtained :  $tested \n"; 
  my $exitval = $? >> 8;
  print "test exited with value $exitval\n";
} elsif ( $nominal ne $tested ) {
  print "$reg_output\n";
  print "\n##teamcity[testFailed name='$title' message='regression detected' details='$tc_output' expected='$nominal' actual='$tested'] \n";
  print "Expected :  $nominal  Obtained :  $tested \n";
} else {
#   print "##teamcity[buildStatisticValue key='testDuration' value='@tested[2]']\n";
#   print "##teamcity[buildStatisticValue key='testMemory' value='@tested[3]']\n";
  print "Test successful : $title \n";
  print "Control Values/Obtained : \n$title\n$nominal\n$tested\n";
}

print "##teamcity[testFinished name='$title']\n";
