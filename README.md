TiAMo (Timed Automata Model-checker)
====================================
[M. Colange](http://lsv.fr/~colange)  
[O. Sankur](http://people.irisa.fr/Ocan.Sankur/)

TiAMo is also based on the [DBM library from Uppaal](http://people.cs.aau.dk/~adavid/UDBM/index.html) (part of the udbml library),
and on the [timed automata parser library from Uppaal](http://people.cs.aau.dk/~marius/utap/), distributed with TiAMo.
The name of the authors and contributors of these libraries can be found in their
respective folders.
Both libraries have been adapted by M. Colange (essentially to drop dependency on Boost,
and to work with C++11).

TiAMo is developed and funded by the ERC Project [Equalis](http://www.lsv.ens-cachan.fr/~bouyer/equalis/).


Installation Notes
------------------

Requires ocaml and ocamlbuild (you can obtain them through opam).  
Requires libxml2 (for the parser).  
Requires a C++11-compatible compiler (successfully tested with gcc-4.7, gcc-4.8, gcc-4.9 and gcc-5.3.0).

- install [udbml](http://github.com/osankur/udbml.git)
- install TiAMo

The script 'build.sh' retrieves udbml, compiles and installs it (locally), then compiles
TiAMo. Simply run this script, or modify it if you want a custom installation.
