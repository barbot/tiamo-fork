(**
 * To run this test, first compile it as a .cmxs.
 * The easiest way to achieve this is, from the toplevel directory of TiAMo,
 * and after having compiled TiAMo, to run the command:
   * ocamlbuild tests/compiled.cmxs
 * To use the compiled model, run:
   * ./tiamo reach _build/tests/compiled.cmxs
 *)

module Test : Ita.TA =
struct
  type state = int
  type ta =
    {
      init : state;
      clocks : string array;
    }

  (* /!\ clocks are numbered from 1 to nb_clocks *)
  let nb_clocks t = (Array.length t.clocks)
  let string_of_clock t c =
    assert(c >= 1 && c <= (nb_clocks t));
    t.clocks.(c-1)

  let initial_state t = t.init

  let hash_state s = s

  let equal_state s1 s2 = s1 = s2

  let is_target _ _ = false

  let priority_compare s1 s2 = compare s1 s2

  let transitions_from _ _ = []

  let invariant _ _ = []

  let is_urgent_or_committed _ _ = false

  let rate_of_state _ _ = 1

  let lubounds t s =
    Array.make (nb_clocks t) 10,
    Array.make (nb_clocks t) 10

  let global_mbounds t =
    Array.make (nb_clocks t) 10

  let model =
    let cl = Array.make 1 "" in
    cl.(0) <- "c0";
    {
      init = 0;
      clocks = cl;
    }

  let string_of_state _ _ = "state"

  let print_timed_automaton chan _ = Printf.fprintf chan "dummy TA\n"

end

let _ =
  Ita.loadedmodule := Some (module Test)

