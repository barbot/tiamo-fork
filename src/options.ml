open Reachability
open TimedAutomaton
open Dbm

module type OPTIONS =
sig
  type t (* priced or unpriced *)

  module type ITA = TIMED_AUTOMATON with type MDbm.dbm_t = t

  val get_ta : unit -> (module ITA)

  module type PARTIAL_WO =
    functor (Key : Hashtbl.HashedType) ->
      functor (Dbm : BIG_IDBM with type dbm_t = t) ->
        functor (COMP : INCLUSION with type dbm_t := Dbm.Dbm.t) ->
          functor (Best : WaitOrderedType with type t = Key.t * Dbm.Dbm.t * COMP.arg_t * Key.t _path) ->
            WALK_ORDER  with type key = Key.t
                        and type dbm_t := Dbm.Dbm.t
                        and type arg_t := COMP.arg_t

  val walk_order : (module PARTIAL_WO) ref

  module ITA_WO (TA : ITA)
                (COMP : INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA = TA)
                (Best : BEST)
                (Pwo : PARTIAL_WO) :
    WALK_ORDER  with type key = TA.discrete_state
                and type dbm_t := TA.MDbm.Dbm.t
                and type arg_t := COMP.arg_t

  module type ITA_ABSTRACTION = functor (TA : ITA) ->
    ABSTRACTION with type arg_t = TA.timed_automaton * TA.discrete_state
                and type dbm_t := TA.MDbm.Dbm.t

  module type ITA_INCLUSION = functor (TA : ITA) ->
    INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA := TA

  val abstraction : (module ITA_ABSTRACTION) ref
  val inclusion : (module ITA_INCLUSION) ref

  val bestcompare : (module BEST) ref

  type res_t

  module type MC_FUNCTOR = 
    functor (TA : ITA) ->
      functor (ABS : ABSTRACTION
        with type arg_t = TA.timed_automaton * TA.discrete_state
        and type dbm_t := TA.MDbm.Dbm.t) ->
          functor (COMP : INCLUSION
            with type dbm_t := TA.MDbm.Dbm.t
            and module TA := TA) ->
              functor (WO : WALK_ORDER
                with type key = TA.discrete_state
                and type dbm_t := TA.MDbm.Dbm.t
                and type arg_t := COMP.arg_t) ->
  sig
    type res_t
    val reach : TA.timed_automaton -> res_t -> res_t * WO.path
    val to_string : TA.timed_automaton -> res_t * WO.path -> string
  end
  with type res_t = res_t

  module MC : MC_FUNCTOR
  
  val initial_value : res_t ref

  val arguments : (Arg.key * Arg.spec * Arg.doc) list

end

module Option_common (M : sig module Dbm : Dbm.BIG_IDBM type res_t end) =
struct
  module type ITA = TIMED_AUTOMATON with type MDbm.dbm_t = M.Dbm.dbm_t
  
  module type PARTIAL_WO =
    functor (Key : Hashtbl.HashedType) ->
      functor (Dbm : BIG_IDBM with type dbm_t = M.Dbm.dbm_t) ->
        functor (COMP : INCLUSION with type dbm_t := Dbm.Dbm.t) ->
          functor (Best : WaitOrderedType with type t = Key.t * Dbm.Dbm.t * COMP.arg_t * Key.t _path) ->
            WALK_ORDER  with type key = Key.t
                        and type dbm_t := Dbm.Dbm.t
                        and type arg_t := COMP.arg_t
                      
  module type ITA_ABSTRACTION = functor (TA : ITA) ->
    ABSTRACTION with type arg_t = TA.timed_automaton * TA.discrete_state
                and type dbm_t := TA.MDbm.Dbm.t

  module type ITA_INCLUSION = functor (TA : ITA) ->
    INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA := TA

  module ITA_WO (TA : ITA)
                (COMP : INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA = TA)
                (Best : BEST)
                (Pwo : PARTIAL_WO)
    = Pwo(TA.DS)(TA.MDbm)(COMP)(Best(TA)(COMP))

  let walk_order = ref (module Waiting.Walk_BFS : PARTIAL_WO)
  let bestcompare = ref (module HighPrio : BEST)
  
  type res_t = M.res_t

  module type MC_FUNCTOR = 
    functor (TA : ITA) ->
      functor (ABS : ABSTRACTION
        with type arg_t = TA.timed_automaton * TA.discrete_state
        and type dbm_t := TA.MDbm.Dbm.t) ->
          functor (COMP : INCLUSION
            with type dbm_t := TA.MDbm.Dbm.t and module TA := TA) ->
              functor (WO : WALK_ORDER
                with type key = TA.discrete_state
                and type dbm_t := TA.MDbm.Dbm.t
                and type arg_t := COMP.arg_t) ->
  sig
    type res_t
    val reach : TA.timed_automaton -> res_t -> res_t * WO.path
    val to_string : TA.timed_automaton -> res_t * WO.path -> string
  end
  with type res_t = res_t

  let do_bound = ref false

  let get_ta () : (module ITA) =
    match !Ita.loadedmodule with
    | None -> failwith "No module loaded!"
    | Some m ->
        let module BTA = (val m : Ita.TA) in
        let module TA = TimedAutomaton.MakeTimedAutomaton(BTA)(M.Dbm) in
        if !do_bound then
          (module TimedAutomaton.MBoundedAutomaton(TA))
        else
          (module TA)

  (* TODO move common options here *)
  let common_args =
    [
      ( "-witness",
        Arg.Unit (function () -> print_path := true),
        " Print a witness path (not activated by default)");

      ( "-cora",
        Arg.Unit (function () -> Costs.enable_cora := true),
        " Enable CORA-syntax support for priced models (not activated by default)");

      ( "-order",
      Arg.String (function
        | "BFS" -> walk_order := (module Waiting.Walk_BFS : PARTIAL_WO)
        | "DFS" -> walk_order := (module Waiting.Walk_DFS : PARTIAL_WO)
        | "BBFS" -> walk_order := (module Waiting.BBFST : PARTIAL_WO)
        | "BDFS" -> walk_order := (module Waiting.BDFST : PARTIAL_WO)
        | "SBFS" -> walk_order := (module Best_wait.WSTS_WO : PARTIAL_WO)
        | _ -> raise (Arg.Bad "Invalid argument for option -order")),
      " Sets the order in which the RG is explored:
        \tBFS [default] Bread-First Search
        \tDFS           Depth-First Search
        \tBBFS          Best first (i.e. higher preference, see state variable \"preference\"), BFS on equivalent ones
        \t              With optimality analysis (tiamo optimal), also see -best option.
        \tBDFS          Best first (i.e. higher preference, see state variable \"preference\"), DFS on equivalent ones
        \t              With optimality analysis (tiamo optimal), also see -best option.
        \tSBFS          Smart BFS");
      
    ]
end

module Unpriced =
struct
  type t = Udbml_unpriced.Dbm.t

  include Option_common(struct module Dbm = UDbm type res_t = bool end)

  let abstraction = ref (module Extra_LU : ITA_ABSTRACTION)
  let inclusion = ref (module Inclusion : ITA_INCLUSION)
  let initial_value = ref false

  module MC = Reachability

  let arguments = Arg.align (common_args @
    [
      ( "-abstr",
        Arg.String (function
          | "LU" -> abstraction := (module Extra_LU : ITA_ABSTRACTION)
          | "M" -> raise (Arg.Bad "M abstraction is currently not implemented")
          | "ID" -> abstraction := (module ID_abstraction : ITA_ABSTRACTION)
          | _ -> raise (Arg.Bad "Invalid argument for option -abstr")),
        " Sets the abstraction to use:
          \tLU [default]  LU (per discrete state) abstraction
          \tM             M abstraction (unimplemented)
          \tID            No abstraction (use with bounded automata, otherwise
                          there is no guarantee of termination");

      ( "-incl",
        Arg.String (function
          | "simple" -> inclusion := (module Inclusion : ITA_INCLUSION)
          | "sri" -> inclusion := (module Sri : ITA_INCLUSION)
          | _ -> raise (Arg.Bad "Invalid argument for option -incl")),
        " Sets the inclusion to use:
          \tsimple [default]  regular set inclusion
          \tsri               abstract inclusion (Z subset of abs_LU(Z'))");

    ])
end

module Priced =
struct
  type t = Udbml_priced.PDbm.t

  include Option_common(struct module Dbm = PDbm type res_t = int end)

  let abstraction = ref (module ID_abstraction : ITA_ABSTRACTION)
  let inclusion = ref (module Inclusion : ITA_INCLUSION)
  let initial_value = ref Dbm.PDbm.Dbm.infty
  
  module MC = OptReachability

  let arguments = Arg.align (common_args @
    [
      ( "-best",
      Arg.String (function
        | "cost" -> bestcompare := (module BestCost : BEST)
        | "pref" -> bestcompare := (module HighPrio : BEST)
        | _ -> raise (Arg.Bad "Invalid argument for option -best")),
      " Sets the way to chose the best candidate in waiting list (see 'BBSF' and 'BDFS' arguments of option -order):
        \tpref [default]  Choose the symbolic state with the highest preference, encoded in the variable \"preference\" of the model
        \tcost            Choose the symbolic state with the smallest minimal cost (i.e. the most promising one cost-wise)");

      ( "-incl",
      Arg.String (function
        | "simple" -> 
            Log.warning "If your automaton is not acyclic or bounded, the chosen inclusion test does not guarantee termination.\n";
            inclusion := (module Inclusion : ITA_INCLUSION)
        | "exp" ->
            inclusion := (module PricedExp : ITA_INCLUSION)
        | _ -> raise (Arg.Bad "Invalid argument for option -incl")),
      " Sets the inclusion to compare priced zones
        \tsimple [default]  regular priced zone inclusion (termination guaranteed only for acyclic or bounded automata)
        \texp               abstract inclusion, exponential in the number of clocks");

      ( "-p",
      Arg.Unit (function () -> pruning := true),
      " Prunes the state space using the best value known.
        This is sound only if the cost is monotonous (i.e. all costs and rates are non-negative)");

      ( "-hint",
      Arg.Int (fun v0 -> initial_value := v0; pruning := true),
      " Allows to give a known upper bound (an integer) for the sought optimum (implies -p).
        \tThe algorithm uses this hint to prune its search space.
        \tTypically, the given hint is the cost of a path leading to a target state (obtained by an unpriced reachability analysis), or a structural bound of the model.
        \tDefault value is INT_MAX, the largest integer representable on a signed machine word.");

    ])
end

(* TODO ML, O and C are not supported yet *)
type input_type_t = XML | CMXS | ML | O | C
let inputtype = ref XML

(* filenames to load the model *)
let tafile = ref ""
let qfile = ref ""
let ofile = ref ""

let load () =
  match !inputtype with
  | XML -> assert(!tafile <> ""); Ita.loadedmodule := Some (module Uppaalta.GenericUAutomaton(struct let tafile = !tafile;; let qfile = !qfile;; let enable_cora = !Costs.enable_cora end))
  | CMXS ->
    begin
      assert(!ofile <> "");
      try
        Dynlink.loadfile !ofile;
      with
        | Dynlink.Error e -> failwith (Dynlink.error_message e)
    end;
    assert(!Ita.loadedmodule <> None)
  | _ -> failwith "unsupported input format"


let verify (module O : OPTIONS) =
  load ();
  let module TA = (val (O.get_ta ())) in
  let ta = TA.model in
(*  TA.print_timed_automaton stdout ta; *)
  let module WO_tmp = (val !O.walk_order : O.PARTIAL_WO) in
  let module ABS = (val !O.abstraction) in
  let module INCL = (val !O.inclusion) in
  let module B = (val !O.bestcompare) in
  let module WO = O.ITA_WO(TA)(struct include INCL(TA) module TA = TA end)(B)(WO_tmp) in
  let module Verifier = O.MC(TA)(ABS(TA))(INCL(TA))(WO) in
  let result = Verifier.reach ta !O.initial_value in
  Verifier.to_string ta result

