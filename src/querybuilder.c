#include <utap/expressionbuilder.h>

class QueryBuilder : public UTAP::ExpressionBuilder
{
public:
    // constructor
    explicit QueryBuilder(UTAP::TimedAutomataSystem *ta)
    : ExpressionBuilder(ta) {}
    // destructor
    virtual ~QueryBuilder() {}

    void property() override {}

    void
    exprProbaQuantitative(int,Constants::kind_t,bool=false) override
    {
        throw "Probabilistic systems are not supported.";
    }
    void
    exprMitlDiamond (int,int) override
    {
        throw "MITL is not supported";
    }
    void
    exprMitlBox (int,int) override
    {
        throw "MITL is not supported";
    }
    void
    exprSimulate(int,int,int,bool=false,int = 0) override
    {
        throw "simulation is not supported";
    }

    const expression_t &getResult() const
    {
        assert(getExpressions().size() == 1);
        return getExpressions()[0];
    }

};
