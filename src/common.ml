
module StringSet = Set.Make(String)
module IntSet = Set.Make( 
    struct
          let compare = Pervasives.compare
              type t = int
    end )
(** Clocks are given unique indices from 0 to nb_clocks -1.*)
type clock_t = int

module ClockSet = Set.Make( 
struct
  let compare = Pervasives.compare
  type t = clock_t
end )

exception Found
exception DiagonalGuard
(*
type dbm_inequality_type = DBM_STRICT | DBM_WEAK

let string_of_ineq = function
  |DBM_WEAK -> "<="
  |DBM_STRICT -> "<"

(*
type extended_int = Infty | Int of int
let string_of_extint = function
  |Int(a) -> string_of_int a
  |Infty -> "INF"

let add_extint a b = match (a,b) with
  Int(a),Int(b) -> Int(a+b)
  |_,Infty -> Infty
  |Infty,_ -> Infty
*)
let compare_inequality_type a b = match (a,b) with
  DBM_STRICT, DBM_STRICT
  | DBM_WEAK, DBM_WEAK -> 0
  | DBM_STRICT, DBM_WEAK -> -1
  | DBM_WEAK , DBM_STRICT -> 1
*)
