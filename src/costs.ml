open Expression

let enable_cora = ref false

type edge_cost = expression
type loc_rate = expression option

let edge_cost_def = Constant(0)
let loc_rate_def = None

let get_rate rates eval_exp =
  if !enable_cora then
    Array.fold_left (fun c -> function
      | None -> c
      | Some(e) -> c + eval_exp e) 0 rates
  else
    1

