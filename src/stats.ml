let modulo = 5000

(* all states seen *)
let states_seen = ref 0

(* all states whose set of successors has been computed *)
let states_explored = ref 0

(* max number of states in storage at once *)
let max_states = ref 0

(* number of inclusion tests *)
let inclusion_tests = ref 0

(* number of successful inclusion tests *)
let positive_inclusion_tests = ref 0

(* number of discrete states seen *)
let number_of_discrete_states = ref 0

let print_stats () =
  Printf.printf "\n";
  Printf.printf "discrete states:\t%d\n" !number_of_discrete_states;
  Printf.printf "states seen:\t\t%d\n" !states_seen;
  Printf.printf "states explored:\t%d\n" !states_explored;
  Printf.printf "max size of storage:\t%d\n" !max_states;
  Printf.printf "incl tests:\t\t%d\n" !inclusion_tests;
  Printf.printf "pos. incl tests:\t%d\n" !positive_inclusion_tests

