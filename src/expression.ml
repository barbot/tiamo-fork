type expression =
  | Constant of int
  | Variable of int
  | ConstVariable of int
  | Clock of int
  | ClockArray of int * expression list
  | Array of int * expression list
  | ConstArray of int * expression list
  | Sum of expression * expression
  | Product of expression * expression
  | Substraction of expression * expression
  | Division of expression * expression
