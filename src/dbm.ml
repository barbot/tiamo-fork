
module type IDBM =
sig
  type t
  type fed_t

  val create : int -> t
  val set_zero : t -> unit
  val set_init : t -> unit
  val set_empty : t -> unit
  val copy : t -> t
  val hash : t -> int

  val infty : int
  val is_empty : t -> bool
  val leq : t -> t -> bool
  val equal : t -> t -> bool

  val to_fed : t -> fed_t

  val constrain : t -> Udbml.Basic_types.clock_constraint_t -> unit
  val up : t -> int -> fed_t
  val update_value : t -> Udbml.Basic_types.cindex_t -> Udbml.Basic_types.bound_t -> fed_t
  val intersect : t -> Udbml_unpriced.Dbm.t -> unit

  val to_string : t -> string

  (* a function that compares two DBMs and tells which one is best to explore first *)
  val is_best : t -> t -> int
end

module type IFED =
sig
  type t
  type dbm_t

  val from_dbm : dbm_t -> t
  val hash : t -> int

  val is_empty : t -> bool
  val set_empty : t -> unit

  (* TODO replace this with an iterator *)
  val to_dbm : t -> dbm_t list
  val iter : t -> (dbm_t -> unit) -> unit

  val up : t -> int -> unit
  val update_value : t -> Udbml.Basic_types.cindex_t -> Udbml.Basic_types.bound_t -> unit
  val intersect : t -> Udbml_unpriced.Dbm.t -> unit
end

module type BIG_IDBM =
sig
  type dbm_t
  type fed_t

  module type DBM = IDBM with type t = dbm_t and type fed_t := fed_t
  module type FED = IFED with type t = fed_t and type dbm_t := dbm_t

  module Dbm : DBM
  module Fed : FED
end

module UDbm : BIG_IDBM with type dbm_t = Udbml_unpriced.Dbm.t =
struct
  type dbm_t = Udbml_unpriced.Dbm.t
  type fed_t = Udbml_unpriced.Fed.t

  module type DBM = IDBM with type t = dbm_t and type fed_t := fed_t
  module type FED = IFED with type t = fed_t and type dbm_t := dbm_t

  module rec Dbm : DBM =
    struct
      include Udbml_unpriced.Dbm

      let to_fed z = Fed.from_dbm z

      let up z r =
        let f = to_fed z in
        Fed.up f r;
        f

      let update_value z c b =
        let f = to_fed z in
        Fed.update_value f c b;
        f

      let is_best _ _ = 0
    end
  and Fed : FED =
    struct
      include Udbml_unpriced.Fed

      let up z _ = Udbml_unpriced.Fed.up z

      let to_dbm t =
        let res = ref [] in
        iter t (fun z -> res := z :: !res);
        !res

      let intersect t z = intersect_dbm t z
    end
end

module PDbm : BIG_IDBM with type dbm_t = Udbml_priced.PDbm.t =
struct
  type dbm_t = Udbml_priced.PDbm.t
  type fed_t = Udbml_priced.PFed.t

  module type DBM = IDBM with type t = dbm_t and type fed_t := fed_t
  module type FED = IFED with type t = fed_t and type dbm_t := dbm_t

  module rec Dbm : DBM =
    struct
      include Udbml_priced.PDbm
      
      let to_fed z = Fed.from_dbm z

      let up z r =
        let f = to_fed z in
        Fed.up f r;
        f
      
      let update_value z c b =
        let f = to_fed z in
        Fed.update_value f c b;
        f

      let is_best z1 z2 = compare (infimum z1) (infimum z2)
    end
  and Fed : FED =
    struct
      include Udbml_priced.PFed

      let to_dbm t =
        let res = ref [] in
        iter t (fun z -> res := z :: !res);
        !res

      let intersect t z =
        (* if z is empty, act as if the transition was disabled,
         * by returning an empty fed
         *)
        if (Udbml_unpriced.Dbm.is_empty z) then
          set_empty t
        else
          intersect_dbm t z
    end
end

