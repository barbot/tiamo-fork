open Reachability

module Deque =
struct
  exception Empty
  type 'a cell = { content : 'a; mutable next : 'a cell; }
  type 'a t = { mutable length : int; mutable tail : 'a cell; }

  let create () =
    { length = 0; tail = Obj.magic None; }

  let push_front x l =
    if l.length = 0 then (
      let rec cell = {
        content = x;
        next = cell;
      }
      in
      l.length <- 1;
      l.tail <- cell;
    ) else (
      let cell = {
        content = x;
        next = l.tail.next;
      }
      in
      l.length <- l.length + 1;
      l.tail.next <- cell;
    )

  let push_back x l =
    push_front x l;
    l.tail <- l.tail.next

  let pop l =
    if l.length = 0 then raise Empty;
    let res = l.tail.next.content in
    if l.length = 1 then (
      l.tail <- Obj.magic None
    ) else (
      l.tail.next <- l.tail.next.next
    );
    l.length <- l.length - 1;
    res

  let is_empty l = l.length = 0

  let length l = l.length
    
end

module WSTS_WO  (Key : Hashtbl.HashedType)
                (BDbm : Dbm.BIG_IDBM)
                (COMP : INCLUSION with type dbm_t := BDbm.Dbm.t)
                (Best : WaitOrderedType with type t = Key.t * BDbm.Dbm.t * COMP.arg_t * Key.t _path)
  : WALK_ORDER with type key = Key.t and type dbm_t := BDbm.Dbm.t and type arg_t := COMP.arg_t =
struct
  
  module KeyHashtbl = Hashtbl.Make(Key)

  type dbm_list = BDbm.Dbm.t list ref
  type key = KeyHashtbl.key
  (* cf. WALK_ORDER signature *)
  type path = (key * Dbm.UDbm.Dbm.t * ((Common.clock_t * int) list) * key) list
  
  type t = dbm_list KeyHashtbl.t * (key * BDbm.Dbm.t * COMP.arg_t * path) Deque.t

  let create () = KeyHashtbl.create 1024, Deque.create ()

  let find_or_add : dbm_list KeyHashtbl.t -> key -> dbm_list =
    fun h -> fun x ->
      try KeyHashtbl.find h x
      with Not_found ->
        incr Stats.number_of_discrete_states;
        let y = ref [] in
        KeyHashtbl.add h x y;
        y

  let add_to_waiting comp (passed, waiting) (l, z, a, p) =
    let bucket = find_or_add passed l in
    if (not (List.exists (fun x -> comp a z x) !bucket)) then
      begin
        let old_size = List.length !bucket in
        let is_incomp x = not (comp a x z) in
        bucket := z :: (List.filter is_incomp !bucket);
        incr Stats.states_seen;
        let new_size = List.length !bucket in
        if (old_size < new_size) then begin
          (* Subsumes noone, add it BFS-style *)
          Deque.push_back (l, z, a, p) waiting;
          Stats.max_states := !Stats.max_states - old_size + new_size
        end
        else begin
          (* subsumes someone (say x), add it DFS-style, so that its successors,
           * who subsume those of x, are explored before those of x
           *)
          Deque.push_front (l, z, a, p) waiting;
        end
      end

  let is_in_passed _ (passed, _) (l, z, _) =
    let bucket = find_or_add passed l in
    not (List.mem z !bucket)

  let add_to_passed _ (_, waiting) _ =
    (* TODO it is the responsibility of the Stats module to print stat infos *)
    incr Stats.states_explored;
    if ((!Stats.states_explored mod Stats.modulo) = 0) then (
      Printf.printf "we have explored %d states, %d in the waiting list\r" !Stats.states_explored (Deque.length waiting);
      flush stdout
    )

  let get_next (_, waiting) = Deque.pop waiting

  let is_done (_, waiting) = Deque.is_empty waiting

  let waiting_length (_, waiting) = Deque.length waiting

  let clean pred (passed, waiting) =
    let size_before = ref 0 in
    let size_after = ref 0 in
    let predaux z =
      let res = pred z in
      incr size_before;
      if (res) then incr size_after;
      res
    in
    KeyHashtbl.iter (fun _ -> fun bucket -> bucket := List.filter predaux !bucket) passed;
    Printf.printf "cleaning: shrink from %d states to %d states stored\n" !size_before !size_after;
    Printf.printf "still %d states waiting for treatment\n" (Deque.length waiting);
    flush stdout
end


