open Common
open TimedAutomaton
open Varcontext

let print_path = ref false

(** An abstract type for zone abstractions *)
module type ABSTRACTION =
sig
  (* the abstract type for a possible additional argument *)
  type arg_t
  type dbm_t

  val extrapolate : dbm_t -> arg_t -> unit
end

(** ID abstraction
 *)
module ID_abstraction (TA : TIMED_AUTOMATON) :
  ABSTRACTION with type arg_t = TA.timed_automaton * TA.discrete_state
              and type dbm_t := TA.MDbm.Dbm.t =
struct
  type arg_t = TA.timed_automaton * TA.discrete_state

  let extrapolate _ _ = ()
end

(** LU abstraction
 *)
module Extra_LU (TA : TIMED_AUTOMATON with type MDbm.dbm_t = Udbml_unpriced.Dbm.t) :
  ABSTRACTION with type arg_t = TA.timed_automaton * TA.discrete_state
              and type dbm_t := TA.MDbm.Dbm.t =
struct
  type arg_t = TA.timed_automaton * TA.discrete_state

  let extrapolate zone (ta, loc) =
    let (lbounds, ubounds) = TA.lu_bounds ta loc in
    Udbml_unpriced.Dbm.extrapolate_lu_bounds zone lbounds ubounds

end

(** An abstract type for zone inclusion test *)
(** In fact the inclusion module should take responsibility of the storage of
 *  visited zones, as search for a stored zone that compares favorably to a
 *  newly encountered one is crucial performance-wise.
 *)
module type INCLUSION =
sig
  (* the abstract type for a possible additional argument *)
  type arg_t
  type dbm_t
  module TA : TIMED_AUTOMATON

  val inclusion : arg_t -> dbm_t -> dbm_t -> bool

  val get_arg : TA.timed_automaton -> TA.discrete_state -> arg_t
end

(** Vanilla inclusion *)
module Inclusion (TA : TIMED_AUTOMATON) :
  INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA := TA =
struct
  type arg_t = unit

  let inclusion _ = TA.MDbm.Dbm.leq

  let get_arg _ _ = ()
end

(** Smart inclusion test, based on (insert reference here)
 *  According to Theorem 32, we have the following equivalence
 *  (L and U are global, not per location)
 *        not (Z \subseteq a_{LU}(Z'))
 *  iff   \exists x,y   such that   Z_{x,0} \geq (\leq, - U_x)
 *                            and   Z'_{x,y} < Z_{x,y}
 *                            and   Z'_{x,y} + (<, - L_y) < Z_{x,0}
 *)
module Sri (TA : TIMED_AUTOMATON with type MDbm.dbm_t = Udbml_unpriced.Dbm.t) :
  INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA := TA =
struct
  type arg_t = Udbml.Carray.t * Udbml.Carray.t

  let inclusion (lbounds, ubounds) z1 z2 =
    Udbml_unpriced.Dbm.closure_leq lbounds ubounds z1 z2

  let get_arg ta loc = TA.lu_bounds ta loc

end

(** Smart inclusion test for priced zones (my original work)
 *  for every valuation z in the LHS, there exists a z' in the RHS which is
 *  equivalent, and whose cost is greater or arbitrarily close to equal
 *)
module PricedExp (TA : TIMED_AUTOMATON with type MDbm.dbm_t = Udbml_priced.PDbm.t) :
  INCLUSION with type dbm_t := TA.MDbm.Dbm.t and module TA := TA =
struct
  type arg_t = Udbml.Carray.t

  let inclusion mbounds z1 z2 =
    let res = Udbml_priced.PDbm.square_inclusion_exp z1 z2 mbounds in
    (* control => res *)
    assert(
      let control = Udbml_priced.PDbm.leq z1 z2 in
      if (control && (not res)) then
        begin
          (* print the problematic zones *)
          Printf.printf "M = [%s]\n" (Udbml.Carray.to_string mbounds);
          Printf.printf "z1 =\n %s" (Udbml_priced.PDbm.to_string z1);
          Printf.printf "z2 =\n %s" (Udbml_priced.PDbm.to_string z2);
          flush stdout;
          false
        end
      else true);
    res

  let get_arg ta loc = TA.m_bounds ta loc
end

(* a helper type to avoid redundant verbosity in WALK_ORDER implementations *)
type 'a _path = ('a * Dbm.UDbm.Dbm.t * ((clock_t * int) list) * 'a) list

(** An abstract type for the walk order of a graph *)
(** TODO  Operations returning unit are very imperative style...
 *        but efficiency should prevail
 *  TODO  Make a functor that takes a TA and returns a
 *        WALK_ORDER with type dbm_t := TA.Dbm.t
 *)
module type WALK_ORDER =
sig
  type key (* key to store dbm, i.e. discrete part of states *)
  type t
  type arg_t (* type of the additional argument to the comparison function *)

  type dbm_t (* type of dbm *)

  (* a path is a list of transitions
   * a transition is a quadruple of:
     * discrete state (i.e. key)
     * guard, as an unpriced dbm
     * clock updates list
     * discrete state (i.e. key)
   *)
  type path = key _path

  val create : unit -> t
  val add_to_waiting : (arg_t -> dbm_t -> dbm_t -> bool) -> t -> key * dbm_t * arg_t * path -> unit
  val is_in_passed : (arg_t -> dbm_t -> dbm_t -> bool) -> t -> key * dbm_t * arg_t -> bool
  val add_to_passed : (arg_t -> dbm_t -> dbm_t -> bool) -> t -> key * dbm_t * arg_t -> unit
  val get_next : t -> key * dbm_t * arg_t * path
  val is_done : t -> bool

  val clean : (dbm_t -> bool) -> t -> unit
  val waiting_length : t -> int
end

module type WaitOrderedType =
sig
  type t
  val compare : t -> t -> int
end

(** An abstract type for a container to store the nodes yet to explore.
 *  This implements the order in which nodes are explored.
 *  Takes an ordered type as argument, although the comparison does not really
 *  need to be a total order.
 *)
module type WAIT_CONTAINER =
  functor (Elem : WaitOrderedType) ->
sig
  type t

  val create : unit -> t
  val push : Elem.t -> t -> unit
  val pop : t -> Elem.t
  val is_empty : t -> bool
  val length : t -> int
end

module type BEST =
  functor (TA : TIMED_AUTOMATON) ->
    functor (COMP : INCLUSION with type dbm_t := TA.MDbm.Dbm.t) ->
      WaitOrderedType
        with type t = TA.discrete_state * TA.MDbm.Dbm.t * COMP.arg_t * TA.discrete_state _path

(** A module to compare two states in the waiting list by their infimum cost *)
module BestCost : BEST =
  functor (TA : TIMED_AUTOMATON) ->
    functor (COMP : INCLUSION with type dbm_t := TA.MDbm.Dbm.t) ->
struct
  type t = TA.discrete_state * TA.MDbm.Dbm.t * COMP.arg_t * TA.discrete_state _path

  let compare (_,z1,_,_) (_,z2,_,_) = TA.MDbm.Dbm.is_best z1 z2
end

(** A module to compare two states in the waiting list by their priority *)
module HighPrio : BEST =
  functor (TA : TIMED_AUTOMATON) ->
    functor (COMP : INCLUSION with type dbm_t := TA.MDbm.Dbm.t) ->
struct
  type t = TA.discrete_state * TA.MDbm.Dbm.t * COMP.arg_t * TA.discrete_state _path

  let compare (s1,_,_,_) (s2,_,_,_) = TA.priority_compare s1 s2
end


(** Builds an effective walk order, given a WAIT_CONTAINER and an INCLUSION.
 *)
module Walk_Order (ContFun : WAIT_CONTAINER)
                  (Key : Hashtbl.HashedType)
                  (BDbm : Dbm.BIG_IDBM)
                  (COMP : INCLUSION with type dbm_t := BDbm.Dbm.t)
                  (Best : WaitOrderedType with type t = Key.t * BDbm.Dbm.t * COMP.arg_t * Key.t _path)
  : WALK_ORDER with type key = Key.t and type dbm_t := BDbm.Dbm.t and type arg_t := COMP.arg_t =
struct

  module KeyHashtbl = Hashtbl.Make(Key)

  type dbm_list = BDbm.Dbm.t list ref
  type key = KeyHashtbl.key
  (* cf. WALK_ORDER signature *)
  type path = key _path

  module Cont = ContFun(Best)
  
  type t = dbm_list KeyHashtbl.t * Cont.t

  let create () = (KeyHashtbl.create 1024, Cont.create ())

  let find_or_add : dbm_list KeyHashtbl.t -> key -> dbm_list =
    fun h -> fun x ->
      try KeyHashtbl.find h x
      with Not_found ->
        incr Stats.number_of_discrete_states;
        let y = ref [] in
        KeyHashtbl.add h x y;
        y

  let add_to_waiting _ (_, waiting) s = Cont.push s waiting

  let is_in_passed comp (passed, _) (l, z, a) =
    let bucket = find_or_add passed l in
    List.exists (fun x -> comp a z x) !bucket

  let add_to_passed comp (passed, waiting) (l, z, a) =
      (* TODO it is the responsibility of the Stats module to print stat infos *)
      incr Stats.states_explored;
      if ((!Stats.states_explored mod Stats.modulo) = 0) then (
        Printf.printf "we have explored %d states, %d in the waiting list\r" !Stats.states_explored (Cont.length waiting);
        flush stdout
      );
      let bucket = find_or_add passed l in
      let old_size = List.length !bucket in
      let is_incomp x = not (comp a x z) in
      bucket := z :: (List.filter is_incomp !bucket);
      incr Stats.states_seen;
      if (old_size < List.length !bucket) then
        Stats.max_states := !Stats.max_states - old_size + List.length !bucket

  let get_next (_, waiting) = Cont.pop waiting

  let is_done (_, waiting) = Cont.is_empty waiting

  let waiting_length (_, waiting) = Cont.length waiting

  let clean pred (passed, waiting) =
    let size_before = ref 0 in
    let size_after = ref 0 in
    let predaux z =
      let res = pred z in
      incr size_before;
      if (res) then incr size_after;
      res
    in
    KeyHashtbl.iter (fun _ -> fun bucket -> bucket := List.filter predaux !bucket;) passed;
    Printf.printf "cleaning: shrink from %d states to %d states stored\n" !size_before !size_after;
    Printf.printf "still %d states waiting for treatment\n" (Cont.length waiting);
    flush stdout

end

module Walk_Order_Opt (ContFun : WAIT_CONTAINER)
                      (Key : Hashtbl.HashedType)
                      (BDbm : Dbm.BIG_IDBM)
                      (COMP : INCLUSION with type dbm_t := BDbm.Dbm.t)
                      (Best : WaitOrderedType with type t = Key.t * BDbm.Dbm.t * COMP.arg_t * Key.t _path)
  : WALK_ORDER with type key = Key.t and type dbm_t := BDbm.Dbm.t and type arg_t := COMP.arg_t =
struct

  module KeyHashtbl = Hashtbl.Make(Key)

  type dbm_list = BDbm.Dbm.t list ref
  type key = KeyHashtbl.key
  (* cf. WALK_ORDER signature *)
  type path = key _path
  
  module Cont = ContFun(Best)
  type t = dbm_list KeyHashtbl.t * Cont.t

  let create () = (KeyHashtbl.create 1024, Cont.create ())

  let find_or_add : dbm_list KeyHashtbl.t -> key -> dbm_list =
    fun h -> fun x ->
      try KeyHashtbl.find h x
      with Not_found ->
        incr Stats.number_of_discrete_states;
        let y = ref [] in
        KeyHashtbl.add h x y;
        y

  let add_to_waiting comp (passed, waiting) (l, z, a, p) =
    let bucket = find_or_add passed l in
    if (not (List.exists (fun x -> comp a z x) !bucket)) then
      begin
        Cont.push (l, z, a, p) waiting;
        let old_size = List.length !bucket in
        let is_incomp x = not (comp a x z) in
        bucket := z :: (List.filter is_incomp !bucket);
        incr Stats.states_seen;
        if (old_size < List.length !bucket) then
          Stats.max_states := !Stats.max_states - old_size + List.length !bucket
      end

  let is_in_passed _ (passed, _) (l, z, _) =
    let bucket = find_or_add passed l in
    not (List.memq z !bucket)

  let add_to_passed _ (_, waiting) _ =
    (* TODO it is the responsibility of the Stats module to print stat infos *)
    incr Stats.states_explored;
    if ((!Stats.states_explored mod Stats.modulo) = 0) then (
      Printf.printf "we have explored %d states, %d in the waiting list\r" !Stats.states_explored (Cont.length waiting);
      flush stdout
    )

  let get_next (_, waiting) = Cont.pop waiting

  let is_done (_, waiting) = Cont.is_empty waiting

  let waiting_length (_, waiting) = Cont.length waiting

  let clean pred (passed, waiting) =
    let size_before = ref 0 in
    let size_after = ref 0 in
    let predaux z =
      let res = pred z in
      incr size_before;
      if (res) then incr size_after;
      res
    in
    KeyHashtbl.iter (fun _ -> fun bucket -> bucket := List.filter predaux !bucket) passed;
    Printf.printf "cleaning: shrink from %d states to %d states stored\n" !size_before !size_after;
    Printf.printf "still %d states waiting for treatment\n" (Cont.length waiting);
    flush stdout
end


(** Other walk orders will probably require some knowledge about the states,
 *  and will thus be functors of TIMED_AUTOMATON *)
module TA_RG_WALK =
  functor (TA : TIMED_AUTOMATON) ->
    functor (ABS : ABSTRACTION
      with type arg_t = TA.timed_automaton * TA.discrete_state
      and type dbm_t := TA.MDbm.Dbm.t) ->
        functor (COMP : INCLUSION
          with type dbm_t := TA.MDbm.Dbm.t
          and module TA := TA) ->
            functor (WO : WALK_ORDER
              with type key = TA.discrete_state
              and type dbm_t := TA.MDbm.Dbm.t
              and type arg_t := COMP.arg_t) ->
struct

  module Dbm = TA.MDbm.Dbm
  module Fed = TA.MDbm.Fed

  module DSHashtbl = Hashtbl.Make(TA.DS)

  let transitions_cache = DSHashtbl.create 1024

  (** Auxiliary function that computes the list of successors of an
   *  extended state.
   *  Not intended to be called from the outside.
   *  TODO is returning succs in a list the most efficient way?
   *)
  let rec succ_zone ta (loc, zone) path = 
    let transition_list =
      try
        DSHashtbl.find transitions_cache loc
      with
      | Not_found ->
          let tlist0 = TA.transitions_from ta loc in
          let tlist = List.map (fun (s,g,u,t) -> (s,g,u,t,COMP.get_arg ta t)) tlist0
          in
          DSHashtbl.add transitions_cache loc tlist;
          tlist
    in
    List.fold_left
      (apply_single_transition ta (loc, zone) None path)
      [] transition_list

  and apply_single_transition =
    fun ta -> fun (sloc, szone) -> fun global_invariant -> fun path ->
      fun succs_list -> fun (source, guard, resets, target, target_arg) ->
    assert(TA.DS.equal sloc source);
    let result_zone = Dbm.to_fed (Dbm.copy szone) in
    Fed.intersect result_zone guard;
    if (Fed.is_empty result_zone) then
      succs_list
    else (
      List.iter (fun (c, v) -> Fed.update_value result_zone c v) resets;
      if (not (TA.is_urgent_or_committed ta target)) then (
        Fed.up result_zone (TA.rate_of_state ta target);
      );
      Fed.intersect result_zone (TA.invariant_of_discrete_state ta target);
      (match global_invariant with
      | None -> ()
      | Some inv -> Fed.intersect result_zone inv);
      if (Fed.is_empty result_zone) then
        succs_list
      else (
        Fed.iter result_zone (fun z -> ABS.extrapolate z (ta, target));
        let new_path = (source, guard, resets, target) :: path in
        if (TA.is_urgent_or_committed ta target) then
          let tmp = ref [] in
          Fed.iter result_zone (fun z -> tmp := !tmp @ (succ_zone ta (target, z) new_path));
          !tmp @ succs_list
        else
          (target, target_arg, result_zone, new_path) :: succs_list
      )
    )


  (* Auxiliary recursive function, not to call from the outside *)
  (* TODO   how can we early return?
   *        result should belong to an ordered set S,
   *        res0 should be inf S and update_result should be increasing.
   *        if S has a maximal element, we can compare result to it
   *)
  let rec walk_aux dbm_comp wot succs update_result result =
    if (WO.is_done wot) then
      result
    else (
      let (statel, statez, arg, path) = WO.get_next wot in
      let (worth, new_res) = update_result wot result (statel, statez) path in
      if (worth && (not (WO.is_in_passed dbm_comp wot (statel, statez, arg)))) then (
        WO.add_to_passed dbm_comp wot (statel, statez, arg);
        List.iter (fun (loc,arg,fed,trans) ->
          Fed.iter fed (fun z -> WO.add_to_waiting dbm_comp wot (loc, z, arg, trans @ path))) (succs (statel, statez) [])
      );
      walk_aux dbm_comp wot succs update_result new_res
    )

  (* The real function to call *)
  let walk ta succ_zone update_result res0 =
    let dbm_comp a x y =
      incr Stats.inclusion_tests;
      let r = COMP.inclusion a x y in
      if (r) then (incr Stats.positive_inclusion_tests);
      r
    in
    let wot = WO.create () in
    let (init, zinit) = TA.initial_extended_state ta in
    let initfed = Fed.from_dbm zinit in
    Fed.up initfed (TA.rate_of_state ta init);
    Fed.intersect initfed (TA.invariant_of_discrete_state ta init);
    List.iter (fun z ->
        ABS.extrapolate z (ta, init);
        WO.add_to_waiting dbm_comp wot (init, z, COMP.get_arg ta init, [])
      ) (Fed.to_dbm initfed);
    walk_aux dbm_comp wot (succ_zone ta) update_result res0


  (* a helper function to turn a path into a string *)
  let path_to_string ta path = 
    if (!print_path) then
      List.fold_right 
        (fun t -> fun s -> s ^ "\n" ^ (TA.transition_to_string ta t))
        path ""
    else
      ""

end

module Reachability =
  functor (TA : TIMED_AUTOMATON with type MDbm.dbm_t = Udbml_unpriced.Dbm.t) ->
    functor (ABS : ABSTRACTION
      with type arg_t = TA.timed_automaton * TA.discrete_state
      and type dbm_t := TA.MDbm.Dbm.t) ->
        functor (COMP : INCLUSION
          with type dbm_t := TA.MDbm.Dbm.t
          and module TA := TA) ->
            functor (WO : WALK_ORDER
              with type key = TA.discrete_state
              and type dbm_t := TA.MDbm.Dbm.t
              and type arg_t := COMP.arg_t) ->
struct
  type res_t = bool

  module Walker = TA_RG_WALK(TA)(ABS)(COMP)(WO)

  exception Found of WO.path

  let reach ta _ : res_t * WO.path =
    let update_result _ (current_result, _) (l, _) p =
      if (current_result || (TA.is_target ta l)) then
        raise (Found p)
      else
        (true, (false, []))
    in
    let result =
      try
        Walker.walk ta Walker.succ_zone update_result (false, [])
      with Found(path) -> (true, path)
    in
    Stats.print_stats ();
    result

  let to_string ta = function
    | (true,p) -> "Reachable!\nPath is\n" ^ (Walker.path_to_string ta p)
    | (false,_) -> "Not reachable..."
end

let pruning = ref false

module OptReachability =
  functor (TA : TIMED_AUTOMATON with type MDbm.dbm_t = Udbml_priced.PDbm.t) ->
    functor (ABS : ABSTRACTION
      with type arg_t = TA.timed_automaton * TA.discrete_state
      and type dbm_t := TA.MDbm.Dbm.t) ->
        functor (COMP : INCLUSION
          with type dbm_t := TA.MDbm.Dbm.t
          and module TA := TA) ->
            functor (WO : WALK_ORDER
              with type key = TA.discrete_state
              and type dbm_t := TA.MDbm.Dbm.t
              and type arg_t := COMP.arg_t) ->
struct

  module Walker = TA_RG_WALK(TA)(ABS)(COMP)(WO)

  type res_t = int

  let reach ta v0 : res_t * WO.path =
    let update_result wo (current_opt, current_path) = fun (l,z) -> fun p ->
      let inf = Udbml_priced.PDbm.infimum z in
      let worth = if (!pruning) then inf < current_opt else true in
      let res =
        if (TA.is_target ta l && inf < current_opt) then (
          Printf.printf "new optimal value %d replaces %d, found after seeing %d states\n" inf current_opt !Stats.states_explored;
          Printf.printf "corresponding path has length %d\n" (List.length p);
          if (!pruning) then begin
            WO.clean (fun x -> Udbml_priced.PDbm.infimum x < inf) wo;
            (* trigger the GC after cleaning the WO, to reclaim memory *)
            Gc.full_major ()
          end;
          inf, p
        ) else current_opt, current_path
      in (worth, res)
    in
    let result = Walker.walk ta Walker.succ_zone update_result (v0, []) in
    Stats.print_stats ();
    result

  let to_string ta (res,path) =
    if (res = Dbm.PDbm.Dbm.infty) then
      "Not reachable..."
    else
      Printf.sprintf "%d\nPath is\n%s" res (Walker.path_to_string ta path)

end

