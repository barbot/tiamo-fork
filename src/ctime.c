extern "C" {
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/callback.h>
}

#include <chrono>

extern "C" CAMLprim value
time_measure_function(value f)
{
    CAMLparam1(f);
    CAMLlocal1(res);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    res = caml_callback(f, Val_unit);
    end = std::chrono::system_clock::now();

    std::chrono::seconds elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start);

    CAMLreturn(caml_callback2(*caml_named_value("cb_build_pair"), Val_long(elapsed_seconds.count()), res));
}
