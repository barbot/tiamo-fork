
(**
 * Executes a function and time it.
 * The elapsed time is returned in seconds.
 *)
external measure_function : (unit -> 'a) -> int * 'a = "time_measure_function";;

