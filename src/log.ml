open Printf

type log_t = Info | Debug | Warning | Error | Fatal

let log_level = ref Debug
let set_level level = log_level := level


let log_err_channel = stderr
let log_regular_channel = stdout 

let logf level s f = 
  (match level with
	| Error ->
		 Printf.fprintf log_err_channel "[ERROR] ";
		 Printf.fprintf log_err_channel s f
	| Fatal ->
		 Printf.fprintf log_err_channel "[FATAL] ";
		 Printf.fprintf log_err_channel s f;
		 exit(-1)
	| Warning -> 
		 if ( !log_level = Debug || !log_level = Warning ) then
		   (
			 Printf.fprintf log_err_channel "[WARNING] ";
			 Printf.fprintf log_err_channel s f
		   )
	| Info ->
		 if ( !log_level = Info || !log_level = Debug || !log_level = Warning ) then
		   (
			 Printf.fprintf log_regular_channel "[INFO] ";
			 Printf.fprintf log_regular_channel s f
		   )
	| Debug ->
		 if ( !log_level = Debug ) then
		   (
			 Printf.fprintf log_regular_channel "[DEBUG] ";
			 Printf.fprintf log_regular_channel s f;
		   )
  ); 
  flush log_err_channel;
  flush log_regular_channel
					 

let fatal s = 
	logf Fatal "%s" s

let info s =
	logf Info "%s" s
let warning s =
	logf Warning "%s" s
let infof f s =
	logf Info f s
let warningf f s =
	logf Warning f s
let fatalf f s =
	logf Fatal f s

let debug s =
	logf Debug "%s" s

let debugf f s =
	logf Debug f s

let log level s =
	logf level "%s" s


(** Does it belong to log.ml, or rather to a stats.ml? *)
module SearchStatistics =
struct
  let counter_visited = ref 0 
  let counter_gamma = ref 0 
  let counter_acceleration = ref 0
  (** Number of iterations of the binary search *)
  let counter_bsiterations = ref 0
  let increment_visited () = incr(counter_visited)
  let increment_gamma () = incr(counter_gamma)
	let increment_acceleration () = incr(counter_acceleration)
  let reset_counters () =
		counter_visited := 0;
		counter_gamma := 0;
		counter_acceleration := 0;
		counter_bsiterations := 0

(*
  let print_statistics chan mode = 
		match mode with 
			Options.ExactReachability
		| Options.Verifix ->
			fprintf chan "[INF] Search Statistics:\n";
			fprintf chan "[INF] Visited states: %d\n" !counter_visited
		|Options.RobustReachability ->
			fprintf chan "[INF] Search Statistics:\n";
			fprintf chan "[INF] Visited states: %d\n[INF] Expanded gamma cycles: %d (%d new states)\n" !counter_visited !counter_acceleration !counter_gamma
*)
end
