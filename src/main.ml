open Options
open Printf
open Reachability
open TimedAutomaton
open Dbm

(** For Arg.align to properly display help messages, do not forget to make them
 *  start with a space.
 *)
let show_gc_stats = ref false

let anon_arguments arg =
  if Filename.check_suffix arg ".xml" then (
    inputtype := XML;
    tafile := arg
  )
  else if Filename.check_suffix arg ".q" then (
    inputtype := XML;
    qfile := arg
  )
  else if Filename.check_suffix arg ".cmxs" then (
    inputtype := CMXS;
    ofile := arg
  )
  else (
    Log.fatalf "File extension not recognized: %s\n" arg;
    raise (Arg.Bad "Unrecognized file extension")
  )

let usage =
"This is TiAMo, the Timed Automata Model-checker.

Usage: tiamo <command> [options] <ta-file> [<query-file>]

<command> is one of the following:
  reach     performs a reachability analysis
  optimal   performs an optimal reachability analysis
For further information about the options for each command, type
  tiamo <command> -help

<ta-file> is a file describing the input model.
It can be one of the following format:
  - a .xml using uppaal syntax. Note however that TiAMo does not support the full syntax of uppaal.
  - a .cmxs (ocaml shared object file).
Beware of the extension, TiAMo relies on it to decide how to load the model.

<query-file> should be a .q file in uppaal format.
If <ta-file> is not a .xml, this file is simply ignored.
This is for backward compatibility only, as the query can be embedded in the .xml file. 
"

let mc_module = ref (module Unpriced : OPTIONS)

let arg_list = ref []

let set_command arg =
  if !Arg.current = 1 then (
    (match arg with
      | "reach" -> mc_module := (module Unpriced : OPTIONS)
      | "optimal" -> mc_module := (module Priced : OPTIONS)
      | _ as s -> raise (Arg.Bad ("unknown command " ^ s))
    );
    let module MC = (val !mc_module) in
    arg_list := MC.arguments
  ) else anon_arguments arg

let main() = 
  Printexc.record_backtrace true;
  if Array.length Sys.argv < 2 then (
    Arg.usage [] usage;
    exit 1
  );
  Arg.parse_dynamic arg_list set_command usage;
  let module MC = (val !mc_module : OPTIONS) in
  let res = Options.verify (module MC) in
  Printf.printf "Result of verification is %s\n" res;
  if (!show_gc_stats) then
    Gc.print_stat stdout
 
let _ = main ()

