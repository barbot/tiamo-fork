open Expression

type atomic_guard = 
  | GuardLeq of expression * expression
  | GuardLess of expression * expression
  | GuardGeq of expression * expression
  | GuardGreater of expression * expression
  | GuardEqual of expression * expression
  | GuardNeq of expression * expression

(** Atomic guard factory functions, to be registered as callbacks from C *)
(** This ensures that guards are in normal form GuardXX(Clock(_),_) *)
let cb_atomic_guard_LE a = function
  | Clock(_) | ClockArray(_,_) as b -> GuardGeq(b, a)
  | _ as b -> GuardLeq(a,b)
let cb_atomic_guard_LT a = function
  | Clock(_) | ClockArray(_,_) as b -> GuardGreater(b, a)
  | _ as b -> GuardLess(a,b)
let cb_atomic_guard_GE a = function
  | Clock(_) | ClockArray(_) as b -> GuardLeq(b, a)
  | _ as b -> GuardGeq(a,b)
let cb_atomic_guard_GT a = function
  | Clock(_) | ClockArray(_,_) as b -> GuardLess(b, a)
  | _ as b -> GuardGreater(a,b)
let cb_atomic_guard_EQ a = function
  | Clock(_) | ClockArray(_,_) as b -> GuardEqual(b, a)
  | _ as b -> GuardEqual(a,b)
let cb_atomic_guard_NEQ a = function
  | Clock(_) | ClockArray(_,_) as b -> GuardNeq(b, a)
  | _ as b -> GuardNeq(a,b)

let _ =
  Callback.register "cb_atomic_guard_LE" cb_atomic_guard_LE;
  Callback.register "cb_atomic_guard_LT" cb_atomic_guard_LT;
  Callback.register "cb_atomic_guard_GE" cb_atomic_guard_GE;
  Callback.register "cb_atomic_guard_GT" cb_atomic_guard_GT;
  Callback.register "cb_atomic_guard_EQ" cb_atomic_guard_EQ;
  Callback.register "cb_atomic_guard_NEQ" cb_atomic_guard_NEQ


type query =
  | EmptyQuery
  | QueryAnd of query*query
  | QueryOr of query*query
  | Location of int*int
  | Atomic of atomic_guard

(* factory functions for query, to be registered as C callbacks *)
let make_empty_query () = EmptyQuery
let make_query_location procId locId = Location(procId, locId)
let make_query_and q1 q2 = QueryAnd(q1,q2)
let make_query_or q1 q2 = QueryOr(q1,q2)
let make_query_atomic ag = Atomic(ag)

(* Register C callbacks to build a query *)
let _ =
  Callback.register "cb_qb_empty" make_empty_query;
  Callback.register "cb_qb_location" make_query_location;
  Callback.register "cb_qb_make_and" make_query_and;
  Callback.register "cb_qb_make_or" make_query_or;
  Callback.register "cb_qb_make_atomic" make_query_atomic

(* A bunch of utilities callbacks to be used from C *)
let _ =
  let cb_make_none () = None in
  Callback.register "cb_make_None" cb_make_none;
  let cb_make_some a = Some a in
  Callback.register "cb_make_Some" cb_make_some;
  let cb_empty_list () = [] in
  Callback.register "cb_empty_list" cb_empty_list;
  let cb_build_list h t = h::t in
  Callback.register "cb_build_list" cb_build_list;
  let cb_concat_list l1 l2 = l1 @ l2 in
  Callback.register "cb_concat_list" cb_concat_list;
  let build_pair a b = (a,b) in
  Callback.register "cb_build_pair" build_pair

