open Reachability

(**
 * A non-prioritized stack, for pure DFS
 * dummy rewrite of stack interface to match non-polymorphic
 * WAIT_CONTAINER interface *)
module WOStack : WAIT_CONTAINER =
  functor (Elem : WaitOrderedType) ->
struct
  type t = Elem.t Stack.t
  
  let create = Stack.create
  let push = Stack.push
  let pop = Stack.pop
  let is_empty = Stack.is_empty
  let length = Stack.length
end

(**
 * A non-prioritized queue, for pure BFS
 * dummy rewrite of stack interface to match non-polymorphic
 * WAIT_CONTAINER interface *)
module WOQueue : WAIT_CONTAINER =
  functor (Elem : WaitOrderedType) ->
struct
  type t = Elem.t Queue.t

  let create = Queue.create
  let push = Queue.push
  let pop = Queue.pop
  let is_empty = Queue.is_empty
  let length = Queue.length
end

module Walk_DFS = Walk_Order_Opt (WOStack)

module Walk_BFS = Walk_Order_Opt (WOQueue)

(**
 * A custom tree to represent sorted sets, with inplace insertion.
 * Inspired by the representation of Sets in the standard library.
 *)
module MyBTree (T : sig val threshold : int end) : WAIT_CONTAINER =
  functor (Elem : WaitOrderedType) ->
struct
  type btree =
      Null
    | Node of treenode
  and treenode =
  {
    mutable low : btree;
    mutable value : Elem.t;
    mutable high : btree;
    mutable height : int;
  }

  type t = { mutable tree : btree; mutable length : int; }

  exception EmptyTree

  (* balanced tree, the height of the two sons differ by at most 2 *)

  let _height = function
    | Null -> 0
    | Node n -> n.height

  let create () = { tree = Null; length = 0; }

  (* create a singleton tree *)
  let _single x = Node { low = Null; value = x; high = Null; height = 1; }

  let _create l v r =
    let hl = _height l in
    let hr = _height r in
    { low = l; value = v; high = r; height = if hl >= hr then hl+1 else hr+1; } 

  (**
   * We add/remove only one element at a time.
   * So we only need a turn-left and a turn-right procedures.
   * Initially, the height of n may not be up-to-date.
   * The sons are balanced.
   *)
  (* precondition: _height high = _height low + 3 *)
  let _turnleft n =
    assert(_height n.high = _height n.low + 3);
    match n.high with
      | Null -> (*cannot happen*) raise EmptyTree
      | Node right ->
          if _height right.high >= _height right.low then
            begin
              n.low <- Node (_create n.low n.value right.low);
              n.value <- right.value;
              n.high <- right.high;
              n.height <- 1 + (max (_height n.low) (_height n.high));
            end
          else
            begin
              match right.low with
                | Null -> (*cannot happen*) raise EmptyTree
                | Node rleft -> begin
                  n.low <- Node (_create n.low n.value rleft.low);
                  n.value <- rleft.value;
                  n.high <- Node (_create rleft.high right.value right.high);
                  n.height <- 1 + (max (_height n.low) (_height n.high));
                end
            end
    
  (* precondition: _height low = _height high + 3 *)
  let _turnright n =
    assert(_height n.low = _height n.high + 3);
    match n.low with
      | Null -> (*cannot happen*) raise EmptyTree
      | Node left ->
          if _height left.low >= _height left.high then
            begin
              n.high <- Node (_create left.high n.value n.high);
              n.value <- left.value;
              n.low <- left.low;
              n.height <- 1 + (max (_height n.low) (_height n.high));
            end
          else
            begin
              match left.high with
                | Null -> (*cannot happen*) raise EmptyTree
                | Node lright -> begin
                  n.high <- Node (_create lright.high n.value n.high);
                  n.value <- lright.value;
                  n.low <- Node (_create left.low left.value lright.low);
                  n.height <- 1 + (max (_height n.low) (_height n.high));
                end
            end
              
  let rec _push_aux x n =
    (* insert x to the left *)
    if Elem.compare x n.value < T.threshold then begin
      match n.low with
        | Null -> n.low <- _single x
        | Node nl -> _push_aux x nl
    end
    (* insert x to the right *)
    else begin
      match n.high with
        | Null -> n.high <- _single x
        | Node nr -> _push_aux x nr
    end;
    let hl = _height n.low in
    let hr = _height n.high in
    (* rebalance if necessary *)
    (* both sons are balanced
     *  - one is unchanged
     *  - the other one by invariant of _push_aux
     *)
    if hl > hr+2 then
      _turnright n
    else if hr > hl+2 then
      _turnleft n
    else begin
      (* keep height up-to-date *)
      (* NB: it is the responsibility of _turnleft and _turnright to keep
       * height up-to-date in the above branches *)
      n.height <- 1 + (max hl hr)
    end

  let push x t =
    begin
    match t.tree with
      | Null -> t.tree <- _single x
      | Node n -> _push_aux x n
    end;
    t.length <- t.length+1

  let rec _pop_aux n =
    let res = match n.low with
      | Null -> raise EmptyTree
      | Node left when left.low = Null ->
          let r = left.value in
          n.low <- left.high;
          r
      | Node left -> _pop_aux left
    in begin
    (* height n.low may be decreased by one. If so, rebalance *)
      if _height n.low + 2 < _height n.high then
        _turnleft n
      else
        n.height <- 1 + (max (_height n.low) (_height n.high))
    end;
    res

  let pop t =
    match t.tree with
      | Null -> raise EmptyTree
      | Node n when n.low = Null ->
          let res = n.value in
          t.tree <- n.high;
          t.length <- t.length-1;
          res
          (* nothing else to do, balanced *)
      | Node n -> (* the hard case *)
          t.length <- t.length-1;
          _pop_aux n
 
  let length t = t.length
  
  let is_empty t = length t = 0

end

(*
 * A prioritized stack, for prioritized DFS
 * insert an element x before y as soon as x <= y
 * i.e. compare x y < 1
 *)
module Priority_StackTree : WAIT_CONTAINER = MyBTree (struct let threshold = 1 end)

(*
 * A prioritized queue, for prioritized BFS
 * insert an element x before y as soon as x < y
 * i.e. compare x y < 0
 *)
module Priority_QueueTree : WAIT_CONTAINER = MyBTree (struct let threshold = 0 end)

module BDFST = Walk_Order_Opt (Priority_StackTree)

module BBFST = Walk_Order_Opt (Priority_QueueTree)

