// -*- mode: C++; c-file-style: "stroustrup"; c-basic-offset: 4; indent-tabs-mode: nil; -*-

/* libutap - Uppaal Timed Automata Parser.
   Copyright (C) 2002-2003 Uppsala University and Aalborg University.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2.1 of
   the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
*/

#ifndef UTAP_ABSTRACTBUILDER_HH
#define UTAP_ABSTRACTBUILDER_HH

#include <exception>
#include <string>
#include <vector>

#include "utap/builder.h"
#include "utap/utap.h"
#include "utap/position.h"

namespace UTAP
{
    class NotSupportedException : public std::exception
    {
    private:
        std::string error;
    public:
        NotSupportedException(const char *err) { error = err; }
        virtual ~NotSupportedException() throw() {}
        virtual const char* what() const throw() { return error.c_str(); }
    };

    class AbstractBuilder : public ParserBuilder
    {
    protected:
        position_t position;
    public:
        AbstractBuilder();

        virtual void setPosition(uint32_t, uint32_t) override;

        /************************************************************
         * Query functions
         */
        virtual bool isType(const char*) override;

        /************************************************************
         * Types
         */
        virtual void typeDuplicate() override;
        virtual void typePop() override;
        virtual void typeBool(PREFIX) override;
        virtual void typeInt(PREFIX) override;
        virtual void typeDouble(PREFIX) override;
        virtual void typeBoundedInt(PREFIX) override;
        virtual void typeChannel(PREFIX) override;
        virtual void typeClock(PREFIX) override;
        virtual void typeVoid() override;
        virtual void typeScalar(PREFIX) override;
        virtual void typeName(PREFIX, const char* name) override;
        virtual void typeStruct(PREFIX, uint32_t fields) override;
        virtual void typeArrayOfSize(size_t) override;
        virtual void typeArrayOfType(size_t) override;
        virtual void structField(const char* name) override;
        virtual void declTypeDef(const char* name) override;

        /************************************************************
         * Variable declarations
         */
        virtual void declVar(const char* name, bool init) override;
        virtual void declInitialiserList(uint32_t num) override; // n initialisers
        virtual void declFieldInit(const char* name) override; // 1 initialiser

        /********************************************************************
         * Gantt chart declaration
         */
        virtual void ganttDeclStart(const char* name) override;
        virtual void ganttDeclSelect(const char *id) override;
        virtual void ganttDeclEnd() override;
        virtual void ganttEntryStart() override;
        virtual void ganttEntrySelect(const char *id) override;
        virtual void ganttEntryEnd() override;

        /************************************************************
         * Progress measure declarations
         */
        virtual void declProgress(bool) override;

        /************************************************************
         * Function declarations
         */
        virtual void declParameter(const char* name, bool) override;
        virtual void declFuncBegin(const char* name) override; // n paramaters
        virtual void declFuncEnd() override; // 1 block

        /************************************************************
         * Process declarations
         */
        virtual void procBegin(const char* name, const bool isTA = true,
        		const std::string type = "", const std::string mode = "") override;
        virtual void procEnd() override; // 1 ProcBody
        virtual void procState(const char* name, bool hasInvariant, bool hasER) override; // 1 expr
        virtual void procStateCommit(const char* name) override; // mark previously decl. state
        virtual void procStateUrgent(const char* name) override; // mark previously decl. state
        virtual void procStateInit(const char* name) override; // mark previously decl. state
        virtual void procBranchpoint(const char* name) override;
        virtual void procEdgeBegin(const char* from, const char* to, const bool control, const char* actname) override;
        virtual void procEdgeEnd(const char* from, const char* to) override;
        // 1 epxr,1sync,1expr
        virtual void procSelect(const char *id) override;
        virtual void procGuard() override;
        virtual void procSync(Constants::synchronisation_t type) override; // 1 expr
        virtual void procUpdate() override;
        virtual void procProb() override;
        /************************************************************
         * Process declarations for LSC
         */
        virtual void procInstanceLine() override;
        virtual void instanceName(const char* name, bool templ=true) override;
        virtual void instanceNameBegin(const char *name) override;
        virtual void instanceNameEnd(const char *name, size_t arguments) override;
        virtual void procMessage(const char* from, const char* to, const int loc, const bool pch) override;
        virtual void procMessage(Constants::synchronisation_t type) override; // 1 expr
        virtual void procCondition(const std::vector<char*> anchors, const int loc,
                const bool pch, const bool hot) override;
        virtual void procCondition() override; // Label
        virtual void procLscUpdate(const char* anchor, const int loc, const bool pch) override;
        virtual void procLscUpdate() override; // Label
        virtual void hasPrechart(const bool pch) override;

        /************************************************************
         * Statements
         */
        virtual void blockBegin() override;
        virtual void blockEnd() override;
        virtual void emptyStatement() override;
        virtual void forBegin() override; // 3 expr
        virtual void forEnd() override; // 1 stat
        virtual void iterationBegin(const char *name) override; // 1 id, 1 type
        virtual void iterationEnd(const char *name) override; // 1 stat
        virtual void whileBegin() override;
        virtual void whileEnd() override; // 1 expr, 1 stat
        virtual void doWhileBegin() override;
        virtual void doWhileEnd() override; // 1 stat, 1 expr
        virtual void ifBegin() override;
        virtual void ifElse() override;
        virtual void ifEnd(bool) override; // 1 expr, 1 or 2 statements
        virtual void breakStatement() override;
        virtual void continueStatement() override;
        virtual void switchBegin() override;
        virtual void switchEnd() override; // 1 expr, 1+ case/default
        virtual void caseBegin() override;
        virtual void caseEnd() override;  // 1 expr, 0+ stat
        virtual void defaultBegin() override;
        virtual void defaultEnd() override; // 0+ statements
        virtual void exprStatement() override; // 1 expr
        virtual void returnStatement(bool) override; // 1 expr if argument is true
        virtual void assertStatement() override;

        /************************************************************
         * Expressions
         */
        virtual void exprTrue() override;
        virtual void exprFalse() override;
        virtual void exprDouble(double) override;
        virtual void exprId(const char * varName) override;
        virtual void exprNat(int32_t) override; // natural number
        virtual void exprCallBegin() override;
        virtual void exprCallEnd(uint32_t n) override; // n exprs as arguments
        virtual void exprArray() override; // 2 expr
        virtual void exprPostIncrement() override; // 1 expr
        virtual void exprPreIncrement() override; // 1 expr
        virtual void exprPostDecrement() override; // 1 expr
        virtual void exprPreDecrement() override; // 1 expr
        virtual void exprAssignment(Constants::kind_t op) override; // 2 expr
        virtual void exprUnary(Constants::kind_t unaryop) override; // 1 expr
        virtual void exprBinary(Constants::kind_t binaryop) override; // 2 expr
        virtual void exprNary(Constants::kind_t, uint32_t num) override;
        virtual void exprScenario(const char* name) override;
        virtual void exprTernary(Constants::kind_t ternaryop, bool firstMissing) override; // 3 expr
        virtual void exprInlineIf() override; // 3 expr
        virtual void exprComma() override; // 2 expr
        virtual void exprDot(const char *) override; // 1 expr
        virtual void exprDeadlock() override;
        virtual void exprForAllBegin(const char *name) override;
        virtual void exprForAllEnd(const char *name) override;
        virtual void exprExistsBegin(const char *name) override;
        virtual void exprExistsEnd(const char *name) override;
        virtual void exprSumBegin(const char *name) override;
        virtual void exprSumEnd(const char *name) override;

        virtual void exprSync(Constants::synchronisation_t type) override;
        virtual void declIO(const char*,int,int) override;

        virtual void exprSMCControl(int) override;
        virtual void exprProbaQualitative(int,Constants::kind_t,Constants::kind_t,double) override;
        virtual void exprProbaQuantitative(int,Constants::kind_t,bool) override;
        virtual void exprProbaCompare(int,Constants::kind_t,int,Constants::kind_t) override;
        virtual void exprProbaExpected(int,const char*) override;
        virtual void exprSimulate(int,int,int,bool,int) override;
        virtual void exprBuiltinFunction1(Constants::kind_t) override;
        virtual void exprBuiltinFunction2(Constants::kind_t) override;

        //MITL
        virtual void exprMitlFormula ( ) override;
        virtual void exprMitlUntil (int,int ) override;
        virtual void exprMitlRelease (int,int) override;
        virtual void exprMitlDisj () override;
        virtual void exprMitlConj () override;
        virtual void exprMitlNext () override;
        virtual void exprMitlAtom () override;

        /************************************************************
         * System declaration
         */
        virtual void instantiationBegin(const char*, size_t, const char*) override;
        virtual void instantiationEnd(
            const char *, size_t, const char *, size_t) override;
        virtual void process(const char*) override;
        virtual void processListEnd() override;
        virtual void done() override;

        /************************************************************
         * Properties
         */
        virtual void property() override;
        virtual void scenario(const char*) override;// LSC
        virtual void parse(const char*) override;// LSC

        /********************************************************************
         * Guiding
         */
        virtual void beforeUpdate() override;
        virtual void afterUpdate() override;


        /********************************************************************
         * Priority
         */
        virtual void beginChanPriority() override;
        virtual void addChanPriority(char separator) override;
        virtual void defaultChanPriority() override;
        virtual void incProcPriority() override;
        virtual void procPriority(const char*) override;

        virtual void declDynamicTemplate (std::string ) override;
        virtual void exprSpawn (int ) override;
        virtual void exprExit () override;
        virtual void exprNumOf () override;

        virtual void exprForAllDynamicBegin (const char*,const char* ) override;
        virtual void exprForAllDynamicEnd (const char* name) override;
        virtual void exprExistsDynamicBegin (const char*,const char*) override;
        virtual void exprExistsDynamicEnd (const char* name) override;
        virtual void exprSumDynamicBegin (const char*,const char* ) override;
        virtual void exprSumDynamicEnd (const char* ) override;
        virtual void exprForeachDynamicBegin (const char*,const char* ) override;
        virtual void exprForeachDynamicEnd (const char* name) override;
        virtual void exprDynamicProcessExpr (const char*) override;
        virtual void exprMITLForAllDynamicBegin (const char* ,const char*) override;
        virtual void exprMITLForAllDynamicEnd (const char* name) override;
        virtual void exprMITLExistsDynamicBegin (const char* ,const char*) override;
        virtual void exprMITLExistsDynamicEnd (const char* name) override;

        /** Verification queries */
        virtual void queryBegin() override;
        virtual void queryFormula(const char* formula, const char* location) override;
        virtual void queryComment(const char* comment) override;
        virtual void queryEnd() override;
    };
}
#endif
