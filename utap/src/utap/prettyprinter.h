// -*- mode: C++; c-file-style: "stroustrup"; c-basic-offset: 4; indent-tabs-mode: nil; -*-

/* libutap - Uppaal Timed Automata Parser.
   Copyright (C) 2002-2003 Uppsala University and Aalborg University.
   
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2.1 of
   the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
*/

#ifndef UTAP_PRETTYPRINTER_H
#define UTAP_PRETTYPRINTER_H

#include <string>
#include <vector>
#include <ostream>
#include <stack>
#include "utap/abstractbuilder.h"

namespace UTAP
{

    class PrettyPrinter : public AbstractBuilder
    {
    private:
        std::vector<std::string> st;
        std::stack<std::string> type;
        std::stack<std::string> array;
        std::vector<std::string> fields;
        std::stack<std::ostream *> o;
        std::set<std::string> types;
        std::string urgent;
        std::string committed;
        std::string param;
        std::string templateset;
        int select, guard, sync, update;

        bool first;
        uint32_t level;

        void indent();
        void indent(std::string &s);

    public:
        PrettyPrinter(std::ostream &stream);

        virtual void addPosition(
            uint32_t position, uint32_t offset, uint32_t line, std::string path) override;

        virtual void handleError(std::string) override;
        virtual void handleWarning(std::string) override;

        virtual bool isType(const char *) override;
        virtual void typeBool(PREFIX) override;
        virtual void typeInt(PREFIX) override;
        virtual void typeDouble(PREFIX) override;
        virtual void typeBoundedInt(PREFIX) override;
        virtual void typeChannel(PREFIX) override;
        virtual void typeClock();
        virtual void typeClock(PREFIX) override;
        virtual void typeVoid() override;
        virtual void typeScalar(PREFIX) override;
        virtual void typeName(PREFIX, const char *type) override;
        virtual void typePop() override;
        virtual void typeDuplicate() override;
        virtual void typeArrayOfSize(size_t n) override;
        virtual void typeArrayOfType(size_t n) override;
        virtual void typeStruct(PREFIX prefix, uint32_t n) override;
        virtual void structField(const char* name) override;
        virtual void declTypeDef(const char* name) override;
        virtual void declVar(const char *id, bool init) override;
        virtual void declInitialiserList(uint32_t num) override;
        virtual void declFieldInit(const char* name) override;
        virtual void declParameter(const char* name, bool) override;
        virtual void declFuncBegin(const char* name) override;
        virtual void declFuncEnd() override;
        virtual void blockBegin() override;
        virtual void blockEnd() override;
        virtual void emptyStatement() override;
        virtual void forBegin() override;
        virtual void forEnd() override;
        virtual void iterationBegin(const char *name) override; // 1 id, 1 type
        virtual void iterationEnd(const char *name) override; // 1 stat
        virtual void whileBegin() override;
        virtual void whileEnd() override;
        virtual void doWhileBegin() override;
        virtual void doWhileEnd() override;
        virtual void ifBegin() override;
        virtual void ifElse() override;
        virtual void ifEnd(bool) override;
        virtual void breakStatement() override;
        virtual void continueStatement() override;
        virtual void exprStatement() override;
        virtual void returnStatement(bool hasValue) override;
        virtual void procBegin(const char* name, const bool isTA = true,
        		const std::string type = "", const std::string mode = "") override;
        void procState(const char *id, bool hasInvariant);
        virtual void procState(const char *id, bool hasInvariant, bool hasExpRate) override;
        virtual void procStateUrgent(const char *id) override;
        virtual void procStateCommit(const char *id) override;
        virtual void procStateInit(const char *id) override;
        virtual void procSelect(const char *id) override;
        virtual void procGuard() override;
        virtual void procSync(Constants::synchronisation_t type) override;
        virtual void procUpdate() override;
        void procEdgeBegin(const char *source, const char *target, const bool control);
        virtual void procEdgeBegin(const char *source, const char *target, const bool control, const char* actname) override;
        virtual void procEdgeEnd(const char *source, const char *target) override;
        virtual void procEnd() override;
        virtual void exprId(const char *id) override;
        virtual void exprNat(int32_t n) override;
        virtual void exprTrue() override;
        virtual void exprFalse() override;
        virtual void exprCallBegin() override;
        virtual void exprCallEnd(uint32_t n) override;
        virtual void exprArray() override;
        virtual void exprPostIncrement() override;
        virtual void exprPreIncrement() override;
        virtual void exprPostDecrement() override;
        virtual void exprPreDecrement() override;
        virtual void exprAssignment(Constants::kind_t op) override;
        virtual void exprUnary(Constants::kind_t op) override;
        virtual void exprBinary(Constants::kind_t op) override;
        virtual void exprNary(Constants::kind_t op, uint32_t num) override;
        virtual void exprScenario(const char* name) override;
        virtual void exprTernary(Constants::kind_t op, bool) override;
        virtual void exprInlineIf() override;
        virtual void exprComma() override;
        virtual void exprDot(const char *) override;
        virtual void exprDeadlock() override;
        virtual void exprForAllBegin(const char *name) override;
        virtual void exprForAllEnd(const char *name) override;
        virtual void exprExistsBegin(const char *name) override;
        virtual void exprExistsEnd(const char *name) override;
        virtual void exprSumBegin(const char *name) override;
        virtual void exprSumEnd(const char *name) override;
        virtual void exprProba(bool,int,double,int);
        virtual void exprProba2(bool,int);
        virtual void exprProbaQuantitative(int,Constants::kind_t,bool) override;
        virtual void exprMitlDiamond (int,int) override;
        virtual void exprMitlBox (int,int) override;
        virtual void exprSimulate(int,int,int,bool,int) override;
        virtual void beforeUpdate() override;
        virtual void afterUpdate() override;
        virtual void instantiationBegin(const char *, size_t, const char *) override;
        virtual void instantiationEnd(const char *, size_t, const char *, size_t) override;
        virtual void process(const char *id) override;
        virtual void processListEnd() override;
        virtual void done() override;

        /** Verification queries */
        virtual void queryBegin() override;
        virtual void queryFormula(const char* formula, const char* location) override;
        virtual void queryComment(const char* comment) override;
        virtual void queryEnd() override;
    };
}

#endif
