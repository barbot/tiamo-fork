#!/bin/sh

INSTALL_DIR=$PWD/local

# get UDBML
git clone https://github.com/osankur/udbml.git
cd udbml
autoreconf -vfi
./configure --prefix=$INSTALL_DIR CFLAGS="$CFLAGS" CPPFLAGS=$CPPFLAGS LDFLAGS=$LDFLAGS
make
make install
cd ..

# build tiamo
autoreconf -vfi
./configure --with-udbml=$INSTALL_DIR OCAMLBUILDFLAGS="-X local -X udbml"
make clean
make
